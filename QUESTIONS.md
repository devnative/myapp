

## 1. What would you add to your solution if you had more time?
I think the first things I will go for would be more features for the order book to handle different currencies, grouping and better animation effects. I would try also to implement sagas, maybe it will be more efficient from performance perspective.
## 2. What would you have done differently if you knew this page was going to get thousands of views per second vs per week? 
In my opinion the apps should be built from the beginning as scalable as possible. I wouldn’t build something different in those two compared scenarios. I will treat the app as it can get thousands of views per second.
## 3. What was the most useful feature that was added to the latest version of your chosen language?  Please include a snippet of code that shows how you've used it. 
As for React-Native, adding TypeScript and hooks. As for projects, they are under NDAs so I cannot share code snippets, but two nice things to mention are: AI tracking on camera view with audio feedback and another one would be a seamless login feature which was detecting phone number and logging in based on that.
## 4. How would you track down a performance issue in production? Have you ever had to do this? 
Usually I track performance with the debugging tools / performance monitor we have for React Native. But most of the issues I had in production were backend related and I was tracking them from different tools like Grafana etc.
## 5. Can you describe common security concerns to consider for a frontend developer? 
Maybe some of them don’t apply for React-Native, but from what I learned as a front end dev, there are some common sense security matters to take care of like: using catch for avoiding DoS attacks, enable XSS protection mode, be careful with forms/hidden inputs/autocompletes, SQL injection, cross-site scripting / CORS issues, insecure deserializations and also to be careful about displaying sensitive data or storing sensitive data in cookies/localstorage.
## 6. How would you improve the Kraken API that you just used?
It is hard to say what would I improve right now, because I barely tested/used it so I cannot have too much input.


