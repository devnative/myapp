import { Dispatch, Middleware, MiddlewareAPI } from 'redux';
import * as actions from '../types/socketTypes';
import OrderSocket from './OrderSocket';

const Socket = (): Middleware => {

  const orderSocket = new OrderSocket();

  const socketActionHandlers = {
    [actions.SOCKET_OPEN]: orderSocket.connect,
    [actions.SOCKET_CLOSE]: orderSocket.disconnect,
  };

  return (store: MiddlewareAPI) =>
    (next: Dispatch<actions.ActionSocket>) =>
      (action: actions.ActionSocket) => {

        const { dispatch } = store;
        const { type, payload } = action;

        if (type) {
          const handler = Reflect.get(socketActionHandlers, type);

          if (handler) {
            handler(dispatch, payload);
          } else {
            return next(action);
          }
        }
      };
};

export default Socket;