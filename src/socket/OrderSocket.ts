import { Dispatch } from 'redux';
import { SocketParams } from '../types/socketTypes';
import { updateData, updateStatus } from '../redux/actions/OrderBookActions';
import { ConnectionStatus } from '../types/socketTypes';

export default class OrderSocket {

  private reconnectIntervalInMillis = 10000;

  private socket: WebSocket | null = null;
  private reconnectionInterval: NodeJS.Timeout | null = null;

  connect = (dispatch: Dispatch, payload: SocketParams): void => {
    this.disconnect();

    const { url } = payload;

    this.socket = new WebSocket(url);

    this.socket.onopen = () => this.handleOpen(dispatch, payload);
    this.socket.onclose = () => this.handleClose(dispatch);
    this.socket.onerror = () => this.handleError(dispatch, payload);
    this.socket.onmessage = (message: WebSocketMessageEvent) => this.handleMessage(dispatch, message);
  }

  disconnect = (): void => {
    this.socket?.close();
  }

  sendMessage = (message: string): void => {
    this.socket?.send(message);
  }

  private handleOpen = (dispatch: Dispatch, payload: SocketParams) => {
    dispatch(updateStatus(ConnectionStatus.CONNECTED));
    this.clearReconnectInterval();
    this.subscribeToPairs(payload);
  }

  private handleClose = (dispatch: Dispatch) => {
    dispatch(updateStatus(ConnectionStatus.DISCONNECTED));
  }

  private handleError = (dispatch: Dispatch, payload: SocketParams) => {
    if (this.reconnectionInterval === null) {

      dispatch(updateStatus(ConnectionStatus.DISCONNECTED));

      this.reconnectionInterval = setInterval(() => {

        this.connect(dispatch, payload);

      }, this.reconnectIntervalInMillis);
    }
  }

  private handleMessage = (distpatch: Dispatch, messageEvent: WebSocketMessageEvent) => {
    if (messageEvent.data !== undefined) {

      const message = JSON.parse(messageEvent.data);
      const {bids, asks} = message;
      const bidsAsks = {bids, asks};
      distpatch(updateData(bidsAsks));
    }
  }

  private clearReconnectInterval = () => {
    if (this.reconnectionInterval) {
      clearInterval(this.reconnectionInterval);
    }
  }

  private subscribeToPairs = () => {
    const orderMessage = {
      event: 'subscribe',
      feed: 'book_ui_1',
      product_ids: [  
          "PI_XBTUSD",
      ],
    };
    this.sendMessage(JSON.stringify(orderMessage));
  }
}