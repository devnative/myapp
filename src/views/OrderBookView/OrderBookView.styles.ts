import {
  StyleSheet,
} from 'react-native';
import { Colors, Sizes } from '../../utils/theming';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColorWhite,
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxContainer: {
    backgroundColor: Colors.blueBg,
    width: Sizes.widthThreeQuarters,
    padding: Sizes.gutterSize,
    borderRadius: Sizes.gutterSize,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    minHeight: 300,
  },
  titlesRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: Sizes.gutterSize * 1.5,
    letterSpacing: 2,
    textTransform: 'uppercase',
  },
  boxTitleContainer: {
    borderBottomColor: Colors.backgroundColorWhite,
    borderBottomWidth: 0.2,
    marginBottom: Sizes.gutterSize * 0.5,
  },
  boxTitle: {
    fontSize: Sizes.usualText,
    color: Colors.backgroundColorWhite,
    marginBottom: Sizes.gutterSize * 0.5,
  },
  dataRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  titles: {
    color: Colors.fadeWhite,
    fontSize: Sizes.title,
    marginBottom: Sizes.gutterSize * 0.5,
  },
  dataPriceBid: {
    color: Colors.red,
    fontSize: Sizes.usualText,
  },
  dataPriceAsk: {
    color: Colors.green,
    fontSize: Sizes.usualText,
  },
  dataStyle: {
    color: Colors.backgroundColorWhite,
    fontSize: Sizes.usualText,
    marginLeft: 50,
  },
  separator: {
    marginVertical: Sizes.gutterSize,
    height: 1,
    color: Colors.backgroundColorWhite,
    width: '100%',
  }
});