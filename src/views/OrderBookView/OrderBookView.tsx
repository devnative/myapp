import React, { useEffect } from 'react';
import {
  SafeAreaView,
  StatusBar,
  Text,
  useColorScheme,
  View,
  FlatList,
} from 'react-native';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../types/stateTypes';
import { SocketParams } from '../../types/socketTypes';
import { openSocket, closeSocket } from '../../redux/actions/OrderBookActions';
import { SocketUrl } from '../../utils/constants';
import CurrencyFormatter from '../../utils/formatCurrency';
import { OrderData, CurrencyCodes } from '../../types/orderTypes';
import { styles } from './OrderBookView.styles';

const OrderBookView = ({ currency = CurrencyCodes.USD }) => {
  const dispatch = useDispatch();
  const isDarkMode = useColorScheme() === 'dark';
  const bidsData = useSelector(({ socketState }: RootState) => socketState.orders.bids);
  const connectionStatus = useSelector(({ socketState }: RootState) => socketState.connectionStatus);
  const asksData = useSelector(({ socketState }: RootState) => socketState.orders.asks);
  let formatter = new CurrencyFormatter(currency);

  useEffect(() => {

    startConnection();

    return (() => {
      stopConnection();
    });
  }, []);

  useEffect(() => {
    startConnection();
  }, []);

  const startConnection = () => {

    const payload: SocketParams = {
      url: SocketUrl,
    };

    dispatch(openSocket(payload));
  };

  const stopConnection = () => {
    dispatch(closeSocket());
  };

  const PriceComponent = ({ item }: { item: OrderData }, type:string) => {
    const formattedPrice = formatter.format(item[0]);
    return (
      <View style={styles.dataRow}>
        <Text style={type === 'bid' ? styles.dataPriceBid : styles.dataPriceAsk}>{formattedPrice}</Text>
        <Text style={styles.dataStyle}>{item[1]}</Text>
        <Text style={styles.dataStyle}></Text>
      </View>
    );
  };

  const renderSkeleton = (): React.ReactNode => {
    return (
      <SkeletonPlaceholder>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center">
          <SkeletonPlaceholder.Item width={300} height={300} borderRadius={16} />
        </SkeletonPlaceholder.Item>
      </SkeletonPlaceholder>
    )
  }

  const renderContent = (): React.ReactNode => {
    return (
      <View style={styles.boxContainer}>
        <View style={styles.boxTitleContainer}>
          <Text style={styles.boxTitle}>Order Book</Text>
        </View>
        <View style={styles.titlesRow}>
          <Text style={styles.titles}>Price</Text>
          <Text style={styles.titles}>Size</Text>
          <Text style={styles.titles}>Total</Text>
        </View>
        <FlatList
          data={bidsData}
          renderItem={(item) => PriceComponent(item, 'bid')}
          initialNumToRender={12}
          keyExtractor={(item, index) => item.toString() + index.toString()}
        />
        <View style={styles.separator} />
        <FlatList
          data={asksData}
          renderItem={(item) => PriceComponent(item, 'ask')}
          initialNumToRender={12}
          keyExtractor={(item, index) => item.toString() + index.toString()}
        />
      </View>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      {!connectionStatus ? renderContent() : renderSkeleton()}
    </SafeAreaView>
  );
};

export default OrderBookView;
