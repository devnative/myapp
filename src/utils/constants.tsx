import { Currency, CurrencyCodes } from '../types/orderTypes';

export const SocketUrl = 'wss://www.cryptofacilities.com/ws/v1';

export const Currencies: Array<Currency> = [{
  value: CurrencyCodes.USD,
  label: 'US Dollar'
}];