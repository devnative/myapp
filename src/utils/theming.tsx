import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;

export const Colors = {
  red: '#ee5253',
  green: '#29baa5',
  backgroundColorWhite: '#fefefe',
  fadeWhite: '#78868e',
  blueBg: '#31426f',
  black: '#000',
};

export const Sizes = {
  title: 12,
  usualText: 14,
  gutterSize: 16,
  screenWidth,
  screenHeight,
  widthHalf: screenWidth * 0.5,
  widthThird: screenWidth * 0.333,
  widthTwoThirds: screenWidth * 0.666,
  widthQuarter: screenWidth * 0.25,
  widthThreeQuarters: screenWidth * 0.75,
}
