import { createStore, applyMiddleware, combineReducers } from 'redux';
import OrderBookReducer from './reducers/OrderBookReducer';
import Socket from '../socket/SocketMiddleWare';
import { composeWithDevTools } from 'redux-devtools-extension';


const rootReducer = combineReducers({
  socketState: OrderBookReducer
});

export const configureStore = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(Socket()),
));