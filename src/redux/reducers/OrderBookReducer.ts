import { SocketState } from '../../types/stateTypes';
import { ActionSocket, SOCKET_UPDATE_DATA, SOCKET_STATUS, ConnectionStatus } from '../../types/socketTypes';

const wsInitialState: SocketState = {
  connectionStatus: ConnectionStatus.DISCONNECTED,
  orders: {
    bids: [],
    asks: [],
  }
};

const OrderBookReducer = (state: SocketState = wsInitialState, action: ActionSocket): SocketState => {
  switch (action.type) {
  case SOCKET_STATUS: {
    return {
      ...state,
      connectionStatus: action.payload.status
    };
  }
  case SOCKET_UPDATE_DATA: {
    const { payload } = action;
    let bids = [];
    let asks = [];
    if(payload.orders.bids !== undefined) {
    if (Object.keys(payload.orders.bids).length !== 0) {
      let pairs = Object.keys(payload.orders.bids)
      for (let i = 0; i < pairs.length; i++) {
        if (payload.orders.bids.length > 0 && payload.orders.bids[i][1] !== 0) {
          bids.push(payload.orders.bids[pairs[i]])
      }
      }
    }
    if (Object.keys(payload.orders.asks).length !== 0) {
      let pairs = Object.keys(payload.orders.asks)
      for (let i = 0; i < pairs.length; i++) {
        if (payload.orders.asks.length > 0 && payload.orders.asks[i][1] !== 0) {
          asks.push(payload.orders.asks[pairs[i]])
      }
      }
    }
  }
  const concatBids = bids.concat(state.orders.bids);
  const lastBids = concatBids.slice(0,9);
  const concatAsks = bids.concat(state.orders.bids);
  const lastAsks = concatAsks.slice(0,9);
    return {
      ...state,
      orders: {
        ...state.orders,
        bids: lastBids,
        asks: lastAsks,
      }
    };
  }
  }

  return state;
};

export default OrderBookReducer;