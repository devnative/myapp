import { OrderData } from '../../types/orderTypes';
import { ConnectionStatus } from '../../types/socketTypes';
import {
  ActionOpenSocket,
  ActionCloseSocket,
  ActionSocketUpdateStatus,
  ActionSocketUpdateData,
  SOCKET_OPEN,
  SOCKET_CLOSE,
  SOCKET_STATUS,
  SOCKET_UPDATE_DATA,
  SocketParams
} from '../../types/socketTypes';

export const openSocket = (payload: SocketParams): ActionOpenSocket => {

  const action: ActionOpenSocket = {
    type: SOCKET_OPEN,
    payload: payload
  };

  return action;
};

export const closeSocket = (): ActionCloseSocket => {

  const action: ActionCloseSocket = {
    type: SOCKET_CLOSE,
    payload: undefined
  };

  return action;
};

export const updateStatus = (status: ConnectionStatus): ActionSocketUpdateStatus => {
  const action: ActionSocketUpdateStatus = {
    type: SOCKET_STATUS,
    payload: {
      status: status
    }
  };

  return action;
};

export const updateData = (orders: OrderData): ActionSocketUpdateData => {
  const action: ActionSocketUpdateData = {
    type: SOCKET_UPDATE_DATA,
    payload: {
      orders,
    }
  };

  return action;
};
