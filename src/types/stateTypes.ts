import { OrderData } from './orderTypes';
import { ConnectionStatus } from './socketTypes';

export interface RootState {
  socketState: SocketState
}

export interface SocketState {
  connectionStatus: ConnectionStatus;
  orders: OrderData,
} 