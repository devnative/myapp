export enum CurrencyCodes {
  USD = 'USD'
}

export type Currency = {
  value: CurrencyCodes;
  label: string;
}

export interface OrderData {
    bids: [],
    asks: [],
}