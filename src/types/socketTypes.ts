import { OrderData } from './orderTypes';
import * as D from 'io-ts/Decoder';
import { pipe } from 'fp-ts/lib/function';

export const SOCKET_OPEN = 'SOCKET_OPEN';
export const SOCKET_CLOSE = 'SOCKET_CLOSE';
export const SOCKET_STATUS = 'SOCKET_STATUS';
export const SOCKET_UPDATE_DATA = 'SOCKET_UPDATE_DATA';

export enum ConnectionStatus {
  CONNECTED = 0,
  DISCONNECTED = 1
}

export interface SocketParams {
  url: string,
}

export interface ActionOpenSocket {
  type: typeof SOCKET_OPEN;
  payload: SocketParams
}

export interface ActionCloseSocket {
  type: typeof SOCKET_CLOSE;
  payload: undefined
}

export interface ActionSocketUpdateStatus {
  type: typeof SOCKET_STATUS,
  payload: {
    status: ConnectionStatus
  }
}

export interface ActionSocketUpdateData {
  type: typeof SOCKET_UPDATE_DATA,
  payload: {
    orders: OrderData
  }
}

export type ActionSocket =
  ActionOpenSocket |
  ActionCloseSocket |
  ActionSocketUpdateStatus |
  ActionSocketUpdateData

// Decoders
const SocketSubscription = D.type({
  event: D.string,
  pair: D.string,
  status: D.string,
});

export const SocketSubscriptionSuccess = pipe(
  SocketSubscription,
  D.intersect(
    D.type({
      status: D.literal('subscribed')
    })
  )
);

export const SocketSubscriptionFailure = pipe(
  SocketSubscription,
  D.intersect(
    D.type({
      status: D.literal('error')
    })
  )
);

export type SocketSubscriptionSuccess = D.TypeOf<typeof SocketSubscriptionSuccess>
export type SocketSubscriptionFailure = D.TypeOf<typeof SocketSubscriptionFailure>

export type SocketMessage = SocketSubscriptionSuccess | SocketSubscriptionFailure

