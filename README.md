This project was bootstrapped with `npx react-native init MyApp --template react-native-template-typescript`.

## Installation guide

You should only need to do in project's main folder:

`npm install` or `yarn`

After that navigate to /ios folder and do:

`pod install`

## Running the app

From command line do:

`npm start`

After that open the MyApp.xcworkspace file from /ios folder and run the app.
