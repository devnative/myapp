import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from './src/redux'
import OrderBookView from './src/views/OrderBookView/OrderBookView';


const App = () => {
  return (
    <Provider store={configureStore}>
      <OrderBookView />
    </Provider>
  );
};

export default App;
